public class mainObjek1 {
    public static void main(String[] args) {
        //Lingkaran
        Lingkaran telapakKanan = new Lingkaran();
        Lingkaran telapakKiri = new Lingkaran();
        Lingkaran tanganKanan = new Lingkaran();
        Lingkaran tanganKiri = new Lingkaran();
        Lingkaran kepala = new Lingkaran();
        telapakKanan.jari = 3;
        telapakKiri.jari = 3;
        tanganKanan.jari = 5;
        tanganKiri.jari = 5;
        kepala.jari = 4;
        //Persegi Panjang
        persegiPanjang kakiKanan = new persegiPanjang();
        persegiPanjang kakiKiri = new persegiPanjang();
        persegiPanjang lenganKanan = new persegiPanjang();
        persegiPanjang lenganKiri = new persegiPanjang();
        persegiPanjang badan = new persegiPanjang();
        kakiKanan.lebar = 3;
        kakiKanan.panjang = 5;
        kakiKiri.lebar = 3;
        kakiKiri.panjang = 5;
        lenganKanan.lebar = 3;
        lenganKanan.panjang = 5;
        lenganKiri.lebar = 3;
        lenganKiri.panjang = 5;
        badan.lebar = 4;
        badan.panjang = 5;

        double hasil = (telapakKanan.Luas() + telapakKiri.Luas() + tanganKanan.Luas() + tanganKiri.Luas() + kepala.Luas()
                        + kakiKanan.Luas() + kakiKiri.Luas() + lenganKanan.Luas() + lenganKiri.Luas() + badan.Luas());
        double tinggi = (kakiKanan.Tinggi() + badan.Tinggi() + kepala.Tinggi());
        System.out.println("==================================");
        System.out.println("Luas Total Robot adalah " + hasil);
        System.out.println("Tinggi Badan Robot adalah " + tinggi);
        System.out.println("==================================");
    }
}
