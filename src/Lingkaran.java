public class Lingkaran {
    public double jari;
    private final double phi = 3.14;
    private double hasilLuas;
    private double hasilTinggi;

    public double Luas(){
        hasilLuas = getLuas();
        return hasilLuas;
    }  
    private double getLuas(){
        hasilLuas = jari*jari*phi;
        return hasilLuas;
    }
    public double Tinggi(){
        hasilTinggi = getTinggi();
        return hasilTinggi;
    }
    public double getTinggi(){
        hasilTinggi = jari + jari;
        return hasilTinggi;
    }
}
